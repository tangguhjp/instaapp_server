const fs = require("fs");

const db = require("../models");

const Post = db.post;
const Comment = db.comment;
const Likes = db.like;

const uploadFiles = async (req, res) => {
  try {
    if (req.file == undefined) {
      return res.send(`You must select a file.`);
    }

    Post.create({
      type: req.file.mimetype,
      name: req.file.originalname,
      caption: req.body.caption,
      userId: req.userId
    }).then((post) => {
      fs.writeFileSync(
        __basedir + "/resources/static/assets/tmp/" + post.name,
        post.data
      );

      return res.send(`File has been uploaded.`);
    });
  } catch (error) {
    console.log(error);
    return res.send(`Error when trying upload images: ${error}`);
  }
};

const deletePost = async (req, res) => {
  try{
    Post.destroy({
      where: {
        id: req.params.postId,
        userId: req.userId
      },
      truncate: true
    }).then((post) => {

    }).catch((err) => {
      return res.send(`Error when trying delete post`);
    })
  }catch(error){

  }
}

const addComment = async (req, res) => {
    try {
        Comment.create({
          postId: req.params.postId,
          userId: req.userId,
          name: "DDD",
          text: req.body.text,
        }).then((post) => {
          return res.send(`Comment has been added.`);
        });
      } catch (error) {
        console.log(error);
        return res.send(`Error when trying upload images: ${error}`);
      }
};

const deleteComment = async (req, res) => {
  try{
    Comment.destroy({
      where: {
        id: req.params.commentId,
        userId: req.userId
      },
      // truncate: true
    }).then((comment) => {
      if(comment){
        return res.send(`Success`);
      }else{
        return res.send(`Failed`);
      }
    }).catch((err) => {
      return res.send(`Error when trying delete post`);
    })
  }catch(error){
    return res.send(`Error when trying delete comment`)
  }
}

const like = async (req, res) => {
    let data = {
        "postId": req.params.postId,
        "userId": req.userId
    };

    try {
        Likes.findOrCreate({
            where: data
        }).then((like) => {
          if (!like) {
            data.likes = 1;
            Likes.create(data);
          }else{
            Likes.update(
              { likes: 1 },
              { where: data }
            ).then((result) => {
              return res.send(`Likes has been added.`);
            }).catch((err) => {
              return res.send(err);
            })
          }
          return res.send(`Likes has been added.`);
        });
      } catch (error) {
        console.log(error);
        return res.send(`Error when trying upload images: ${error}`);
      }
};

const unlike = async (req, res) => {
  let data = {
      "postId": req.params.postId,
      "userId": req.userId
  };

  try {
      Likes.findOrCreate({
          where: data
      }).then((like) => {
        if (!like) {
          data.likes = 0;
          Likes.create(data);
        }else{
          Likes.update(
            { likes: 0 },
            { where: data }
          ).then((result) =>{
            return res.send(`Likes has been added.`);
          }).catch((err) => {
            return res.send(err);
          })
        }
        return res.send(`Likes has been added.`);
      });
    } catch (error) {
      console.log(error);
      return res.send(`Error when trying upload images: ${error}`);
    }
};

module.exports = {
  uploadFiles,
  deletePost,
  addComment,
  deleteComment,
  like,
  unlike
};