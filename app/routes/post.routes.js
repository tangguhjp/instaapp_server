const { authJwt } = require("../middleware");
const controller = require("../controllers/post.controller");

const upload = require("../middleware/upload");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post("/api/post/upload",
    upload.single("file_photo"),
    [authJwt.verifyToken], 
    controller.uploadFiles
  );
  
  app.post("/api/post/:postId/addComment",
    [authJwt.verifyToken], 
    controller.addComment
  );

  app.post("/api/post/deleteCommment/:commentId",
    [authJwt.verifyToken], 
    controller.deleteComment
  );

  app.post("/api/post/:postId/like",
    [authJwt.verifyToken], 
    controller.like
  );

  app.post("/api/post/:postId/unlike",
    [authJwt.verifyToken], 
    controller.unlike
  );
};