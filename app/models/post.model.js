module.exports = (sequelize, Sequelize) => {
    const Post = sequelize.define("posts", {
        type: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        },
        caption: {
            type: Sequelize.STRING
        }
    });

    return Post;
};