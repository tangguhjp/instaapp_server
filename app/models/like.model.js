module.exports = (sequelize, Sequelize) => {
    const Like = sequelize.define("post_like", {
        likes: {
            type: Sequelize.INTEGER
        }
    });

    return Like;
};